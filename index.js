class Person {
  constructor(name, age, nationality, address) {
    this.name = name;
    this.nationality = nationality;
    this.address = address;
    if (typeof age === `number`) {
      if (age >= 18) {
        return (this.age = age);
      } else {
        return (this.age = undefined);
      }
    } else {
      return (this.age = undefined);
    }
  }

  greeting() {
    console.log(`Hello! Good morning!`);
    return this;
  }

  introduce(name) {
    console.log(`Hello! My name is ${this.name}`);
    return this;
  }

  changeAddress(address) {
    console.log(`${this.name} now lives is ${address}`);
    return this;
  }
}

const person = new Person(`Sarah`, 20, `Filipina`, `123 gaga st`);

const person1 = new Person(`Zion Barlis`, 15, `American`, `855 NY DR`);

console.log(person1);

/*
Mini- Quiz:
1. What is the blueprint where objects are created from?
        Answer: Class


2. What is the naming convention applied to classes?
        Answer: Uppercase


3. What keyword do we use to create objects from a class?
        Answer: new


4. What is the technical term for creating an object from a class?
        Answer: Instantiation


5. What class method dictates HOW objects will be created from that class?
          Answer: Constructor

*/

class Student {
  constructor(name, email, grades) {
    (this.name = name), (this.email = email);

    // Check if the array has 4elements
    if (grades.length === 4) {
      if (grades.every((grade) => grade >= 0 && grade <= 100)) {
        this.grades = grades;
      } else {
        this.grades = undefined;
      }
    } else {
      this.grades = undefined;
    }
    this.gradeAve = undefined;
    this.passed = undefined;
    this.passedWithHonors = undefined;
    this.greeting = undefined;
    this.introduce = undefined;
  }

  login(email) {
    console.log(`${this.email} has logged in`);
    return this;
  }
  logout(email) {
    console.log(`${this.email} has logged out`);
    return this;
  }
  computeAve() {
    let sum = 0;
    this.grades.forEach((grade) => (sum = sum + grade));
    this.gradeAve = sum / 4;
    return this;
  }

  willPass() {
    this.passed = this.computeAve().gradeAve >= 85 ? true : false;
    return this;
  }
  willPassWithHonors() {
    if (this.passed) {
      if (this.gradeAve >= 90) {
        this.passedWithHonors = true;
      } else {
        this.passedWithHonors = false;
      }
    } else {
      this.passedWithHonors = false;
    }
    return this;
  }
}

const studentOne = new Student(
  `Tony`,
  `starksindustries@gmail.com`,
  [89, 84, 78, 88]
);

const studentTwo = new Student(`Peter`, `spideyman@mail.com`, [78, 82, 79, 85]);

const studentThree = new Student(
  `Wanda`,
  `scarlettMaximoff@mail.com`,
  [87, 89, 91, 93]
);

const studentFour = new Student(
  `Steve`,
  `captainRogers@mail.com`,
  [91, 89, 92, 93]
);

/*
 1. Should class methods be included in the class constructor?
        Answer: No


2. Can class methods be separated by commas?
        Answer: No


3. Can we update an object’s properties via dot notation?
        Answer: Yes


4. What do you call the methods used to regulate access to an object’s properties?
        Answer: Property accessors


5. What does a method need to return in order for it to be chainable?
        Answer: this method. There should be a return value.



1. Modify the class Person and add the following methods:
    a. greet method- the person should be able to greet a certain message
    b. introduce method- person should be be able to state their own name
    c. change address- person should be able to update their address and send a message where he now lives
2. All methods should be chainable. 
3. Refactor the age by having a condition in which the age should only take in a number data type and a legal age of 18, otherwise undefined
4. Send a screenshot of your outputs in Hangouts and link it in Boodle
*/
